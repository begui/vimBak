call pathogen#infect('bundle/{}')
call pathogen#helptags()

"Note, a lot of these options are now defined in the awesome
"'vim-sensible' plugin


set encoding=utf-8

if !has('g:syntax_on')|syntax enable|endif          


set background=dark
if has("gui_running")
  colorscheme solarized 
  autocmd GUIEnter * set vb t_vb=     " Turn off beeping
else
  colorscheme Dim 
endif

set number

set nospell				" check spelling
set mouse=a				" enable mouse

set nowrap				" Turn off line wrap
set showcmd			  " display incomplete commands
set wrapscan		  " when search reaches end of file, go to the beginning.
set showcmd			  " show (partial) command in status line.
set showmatch			" When a bracket is inserted, briefly jump to the matching
                  " one. The jump is only done if the match can be seen on the
                  " screen. The time to show the match can be set with
                  " 'matchtime'.
set ignorecase	  " Ignore case in search patterns. For shorthand, use :set ic and :set noic


set tags=./tags;/ " Looks for tag in current directory or move up towards root until it finds one. 

"================================================================
"================================================================
" FUNCTION DECLARATION
"================================================================
"================================================================

"================================
" Turn off Swap
"================================
function! Begui_setNoSwap()
  set noswapfile			
  set nobackup
  set nowb
endfunction

"================================
" Search 
"================================
function! Begui_use_search()
  set hlsearch        " When there is a previous search pattern, highlight all its matches.
  set smartcase       " Override the 'ignorecase' option if the search pattern contains upper case characters.
endfunction

"================================
"
"================================
function! Begui_tabs()
  set softtabstop=2
  set shiftwidth=2
  set tabstop=2
  set expandtab
  "set noexpandtab
  "set preserveindent
endfunction

"================================
" Syntax highligting for files
"================================
function! Begui_addFiletypes()
  "Oracle proc files
  au BufNewFile,BufRead *.pc set filetype=c
  "OpenGL GLSL stuff
  au BufNewFile,BufRead *.vp,*.fp,*.gp,*.vs,*.fs,*.gs,*.tcs,*.tes,*.cs,*.vert,*.frag,*.geom,*.tess,*.shd,*.gls,*.glsl set filetype=glsl430
endfunction

"================================
" Saves the last line after file is closed
"================================
function! SaveLineState()
  au BufReadPost *
        \ if line("'\"") > 1 && line("'\"") <= line("$") |
        \   exe "normal! g`\"" |
        \ endif
endfunction

"================================
"" Removes the auto preview window when autocomplete  
"================================
function! RemoveAutoCompletionWindow()
  augroup remove_auto_complete_winddow
  " vim 7.4 introduced CompleteDone, this is prefered
  autocmd CompleteDone * pclose
  " vim 7.3 
  " autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
  " autocmd InsertLeave * if pumvisible() == 0|pclose|endif
  augroup END
endfunction

function! NerdSetting()
  let NERDTreeShowBookmarks=1
  let NERDTreeChDirMode=0
  let NERDTreeQuitOnOpen=1
  let NERDTreeMouseMode=2
  let NERDTreeShowHidden=1
  let NERDTreeIgnore=['\.pyc','\~$','\.swo$','\.swp$','\.git','\.hg','\.svn','\.bzr']
  let NERDTreeKeepTreeInNewTab=1
  "let g:nerdtree_tabs_open_on_gui_startup=0
endfunction


function! CVIMSetting()
  let  g:C_UseTool_cmake='yes'
endfunction

function! YCMSetting()
  if has ("win32")  || has ("win32unix") || (v:version < 704)
    let g:loaded_youcompleteme = 1
  else
    let g:ycm_collect_identifiers_from_tags_files=1
    let g:ycm_global_ycm_extra_conf = "~/.vim/ycm_extra_conf.py"
    " hides the syntax highlighting 
    let g:ycm_show_diagnostics_ui=0
  endif
endfunction

function! VIMOver()
  let g:over_enable_auto_noh1search=1
endfunction

function! VIMCppEnhanced()
  let g:cpp_class_scope_highlight=1
  let g:cpp_experimental_template_highlight=1
endfunction

function! VIMMarkdown()
  augroup vim_markdown 
    autocmd BufNewFile,BufReadPost *.md set filetype=markdown
  augroup END
    let g:instant_markdown_autostart = 0
endfunction


function! ClangFormat()
  let g:clang_format#command="clang-format-6.0"
  let g:clang_format#detect_style_file = 1
endfunction

function! JavaComplete2()
  augroup java_complete 
    autocmd FileType java setlocal omnifunc=javacomplete#Complete
  augroup END
  "To enable smart (trying to guess import option) inserting class imports with F4, add:
  nmap <F4> <Plug>(JavaComplete-Imports-AddSmart)
  imap <F4> <Plug>(JavaComplete-Imports-AddSmart)
  "To enable usual (will ask for import option) inserting class imports with F5, add:
  nmap <F5> <Plug>(JavaComplete-Imports-Add)
  imap <F5> <Plug>(JavaComplete-Imports-Add)
  "To add all missing imports with F6:
  nmap <F6> <Plug>(JavaComplete-Imports-AddMissing)
  imap <F6> <Plug>(JavaComplete-Imports-AddMissing)
  "To remove all missing imports with F7:
  nmap <F7> <Plug>(JavaComplete-Imports-RemoveUnused)
  imap <F7> <Plug>(JavaComplete-Imports-RemoveUnused)
endfunction
"================================
" Function Calls
"================================
call Begui_tabs()
call Begui_use_search()
call Begui_setNoSwap()
call Begui_addFiletypes()
call SaveLineState()
call NerdSetting()
call RemoveAutoCompletionWindow()
call CVIMSetting()
call YCMSetting()
call VIMOver()
call VIMCppEnhanced()
call VIMMarkdown()
call ClangFormat()
call JavaComplete2()

"================================================================
"================================================================
" KEY MAPPING
"================================================================
"================================================================
" Map Keys for NerdTree
map <A-RIGHT> <C-W>l
map <A-LEFT> <C-W>h
map <A-UP> <C-W>k
map <A-DOWN> <C-W>j
" Highlights all words on double click 
nnoremap <silent> <2-LeftMouse> :let @/='\V\<'.escape(expand('<cword>'), '\').'\>'<CR>:set hls<CR>
" Remove highlighting ',/'
nmap <silent> ,/ :nohlsearch<CR>
" Toggle tagbar
nmap <F8> :TagbarToggle<CR>

