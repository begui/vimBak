# README - vimBak

NO LONGER INUSE, see my dotfiles instead

## Install

    git clone https://gitlab.com/begui/vimBak.git ~/.vim/
    cd .vim/
    git submodule update --init --recursive


## Create symbolic link

	ln -s ~/.vim/vimrc ~/.vimrc
	ln -s ~/.vim/clang-format ~/.clang-format

## Add to your profile

    alias gvim='gvim > /dev/null 2>&1'
    alias gvimdiff='gvimdiff > /dev/null 2>&1'

## Plugins

Once the steps above are complete, run install the following dependencies

    sudo apt install libpython-dev npm exuberant-ctags clang-format cmake

#####  YouCompleteMe

    ./install.py --clang-completer

##### Instant Markdown

    sudo npm -g install instant-markdown-d

##### ctags

Run the following

    cd /path/to/code
    ctags -f ~/.vim/tags/mycode --tag-relative=yes --recurse --language-force=c++ *
    ctags -f ~/.vim/tags/java -R --language-force=java /opt/java/src


* Add to vimrc:

    set tags=~/.vim/tags/mycode,~/.vim/tags/java ```
